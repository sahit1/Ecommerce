import React, { Component } from 'react'
import SHOPDATA from './shop.data'
import CollectionPreview from '../../components/collection-preview/collectionppreview.component'
class ShopPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            collections: SHOPDATA
        }
    }
    render() {
    const {collections} = this.state
        return (<div>{
            collections.map(({id,...othersProp})=>(
                <CollectionPreview key={id} {...othersProp}/>
            ))
            }</div>)

    }
}

export default ShopPage