import React from 'react';

import ShopPage from './pages/shopPage/shoppage.component '
import HomePage from './pages/homePage/homepage.component';
import Header from './components/header/header.component'
import SigninSignup from './pages/signin-signup/signin-signup.component';

import { auth, createUserProfileDocument } from '../src/firebase/firebase.utils';
import { Switch, Route, Redirect } from 'react-router-dom';

import './App.css';

import { connect } from 'react-redux';

import { setCurrentUser } from './redux/user/user.actions'

class App extends React.Component {

  unSubscribeFromAuth = null
  componentDidMount() {
    const { setCurrentUser } = this.props
    this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      if (userAuth) {
        const userRef = await createUserProfileDocument(userAuth)
        userRef.onSnapshot(snapShot => {
          setCurrentUser({
            id: snapShot.id,
            ...snapShot.data()
          })
        });

      }

      setCurrentUser(userAuth);
    });
  }
  componentWillUnmount() {
    this.unSubscribeFromAuth();
  }

  render() {
    return (
      <div className='body' >
        <Header />
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route exact path='/data' component={ShopPage} />
          <Route exact path='/signin' render={() => this.props.currentUser ? (<Redirect to='/' />) : (<SigninSignup />)} />
        </Switch>
      </div>)
  };
}

const mapStateToProps = ({user}) =>( {
  currentUser : user.currentUser
})
const mapDispatchToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user))
})
export default connect(mapStateToProps, mapDispatchToProps)(App);
